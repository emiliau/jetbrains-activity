def abbreviate(w):
    word_length = len(w)
    if word_length > 10:
        too_long = str(w[0]) + str(word_length - 2) + str(w[word_length - 1])
        print(too_long)
    else:
        print(w)


counter = 0
n = input()

while counter < int(n):
    word = input()
    abbreviate(word)
    counter += 1
