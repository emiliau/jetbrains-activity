import sys

def get_list():
    return list(map(int, sys.stdin.readline().strip().split()))

tests = int(input())
while tests != 0:
    inp = input()
    if inp.strip() == '':
        continue
    size, limit = [int(x) for x in inp.split()]
    p = get_list()
    q = get_list()
    for i in range(size):
        if p[i] + q[size - i - 1] > limit:
            print("No")
            break
        else:
            print("Yes")
        tests = tests - 1